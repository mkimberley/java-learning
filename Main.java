class Main{
    public static void main(String[] args){
        boolean isTrue = true;
        for (int i = 0; i < 5; i++)
        {
            System.out.println("Outer: " + i);
            for (int j =0; j < 5; j++){
                System.out.println("Inner: " + j);
            }
        }
        while(isTrue){
            System.out.println("Is true");
            for (int i =0; i <5; i++){
                System.out.println("looping...");
            }
            isTrue = false;
        }
    }
}